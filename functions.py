#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##############################
# LIBRARIES
##############################
import tqdm
import pandas as pd
import matplotlib.pyplot as plt
import os.path
import json
import pickle
### GENSIM
import gensim
import gensim.corpora as corpora
### SPACY for lemmatization
import spacy
## NLTK
import nltk

from gensim.utils import simple_preprocess
from gensim.models import CoherenceModel
from nltk.corpus import stopwords
from wordcloud import WordCloud

### Initiate loading/download 
nlp = spacy.load('en', disable=['parser', 'ner']) # python3 -m spacy download en
nltk.download('stopwords')
stop_words = stopwords.words('english')

##############################
# PATH HANDLER
##############################
# RESSOURCES
PATH_LDA_MODEL = os.path.join("static","ressources","lda_model")
PATH_LSA_MODEL = os.path.join("static","ressources","lsa_model")
PATH_MALLET_MODEL = os.path.join("static","ressources","mallet_model")
PATH_MALLET_BIN = 'mallet-2.0.8/bin/mallet'
PATH_DATA = os.path.join("static","ressources","data")
PATH_WORDCLOUD = os.path.join("static","img","wordcloud")

##############################
## FONCTIONS PREPROCESS
##############################
def sent_to_words(sentences):
    for sentence in sentences:
        yield (gensim.utils.simple_preprocess(str(sentence), deacc=True))


# Define functions for stopwords, bigrams, trigrams and lemmatization
def remove_stopwords(texts):
    return [[word for word in simple_preprocess(str(doc)) if word not in stop_words] for doc in texts]


def make_bigrams(bigram_mod,texts):
    return [bigram_mod[doc] for doc in texts]


def make_trigrams(trigram_mod,bigram_mod,texts):
    return [trigram_mod[bigram_mod[doc]] for doc in texts]


def lemmatization(texts, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
    texts_out = []
    for sent in texts:
        doc = nlp(" ".join(sent))
        texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
    return texts_out

##############################
## FONCTIONS TUNING
##############################

def compute_coherence_values_ldamallet(mallet_path,dictionary, corpus, texts, limit, start=2, step=3):
    coherence_values = []
    model_list = []
    for num_topics in range(start, limit, step):
        model = gensim.models.wrappers.LdaMallet(mallet_path, corpus=corpus, num_topics=num_topics, id2word=dictionary)
        model_list.append(model)
        coherencemodel = CoherenceModel(model=model, texts=texts, dictionary=dictionary, coherence='c_v')
        coherence_values.append(coherencemodel.get_coherence())

    return model_list, coherence_values

def compute_coherence_values(corpus, dictionary,texts, k, a, b):
    lda_model = gensim.models.LdaMulticore(corpus=corpus,
                                           id2word=dictionary,
                                           num_topics=k,
                                           random_state=100,
                                           chunksize=100,
                                           passes=10,
                                           alpha=a,
                                           eta=b,
                                           per_word_topics=True)

    coherence_model_lda = CoherenceModel(model=lda_model, texts=texts, dictionary=dictionary, coherence='c_v')

    return coherence_model_lda.get_coherence()


def finding_hyperparameters(id2word, corpus_sets,data_lemmatized, topics_range, alpha, beta, result_to_csv):
    model_results = {'Validation_Set': [],
                     'Topics': [],
                     'Alpha': [],
                     'Beta': [],
                     'Coherence': []
                     }

    # Calculate iteration needed
    pbar = tqdm.tqdm(total=len(corpus_sets) * len(topics_range) * len(alpha) * len(beta))

    # iterate through validation corpuses
    for i in range(len(corpus_sets)):
        # iterate through number of topics
        for k in topics_range:
            # iterate through alpha values
            for a in alpha:
                # iterare through beta values
                for b in beta:
                    # get the coherence score for the given parameters
                    cv = compute_coherence_values(corpus=corpus_sets[i], dictionary=id2word,texts=data_lemmatized,
                                                  k=k, a=a, b=b)
                    # Add results
                    model_results['Validation_Set'].append("Corpus "+str(i))
                    model_results['Topics'].append(k)
                    model_results['Alpha'].append(a)
                    model_results['Beta'].append(b)
                    model_results['Coherence'].append(cv)
                    # Progress bar update
                    pbar.update(1)
    # Result to CSV
    if result_to_csv:
        pd.DataFrame(model_results).to_csv('lda_tuning_results.csv', index=False)
    # Close progress bar
    pbar.close()
    return model_results


##############################
## FONCTIONS ANALYSIS
##############################
#---------------------------------------------------------
### ANALYSING TOPICS RESULTS
#---------------------------------------------------------
    
def format_topics_sentences(ldamodel, corpus, texts):
    # Init output
    sent_topics_df = pd.DataFrame()

    # Get main topic in each document
    for i, row in enumerate(ldamodel[corpus]):
        row = sorted(row[0], key=lambda x: (x[1]), reverse=True)
        # Get the Dominant topic, Perc Contribution and Keywords for each document
        for j, (topic_num, prop_topic) in enumerate(row):
            if j == 0:  # => dominant topic
                wp = ldamodel.show_topic(topic_num)
                topic_keywords = ", ".join([word for word, prop in wp])
                sent_topics_df = sent_topics_df.append(pd.Series([int(topic_num), round(prop_topic,4), topic_keywords]), ignore_index=True)
            else:
                break
    sent_topics_df.columns = ['Dominant_Topic', 'Perc_Contribution', 'Topic_Keywords']

    # Add original text to the end of the output
    contents = pd.Series(texts)
    sent_topics_df = pd.concat([sent_topics_df, contents], axis=1)
    return(sent_topics_df)
    
def format_topics_sentences_mallet(ldamodel, corpus, texts):
    # Init output
    sent_topics_df = pd.DataFrame()

    # Get main topic in each document
    for i, row in enumerate(ldamodel[corpus]):
        row = sorted(row, key=lambda x: (x[1]), reverse=True)
        # Get the Dominant topic, Perc Contribution and Keywords for each document
        for j, (topic_num, prop_topic) in enumerate(row):
            if j == 0:  # => dominant topic
                wp = ldamodel.show_topic(topic_num)
                topic_keywords = ", ".join([word for word, prop in wp])
                sent_topics_df = sent_topics_df.append(pd.Series([int(topic_num), round(prop_topic,4), topic_keywords]), ignore_index=True)
            else:
                break
    sent_topics_df.columns = ['Dominant_Topic', 'Perc_Contribution', 'Topic_Keywords']

    # Add original text to the end of the output
    contents = pd.Series(texts)
    sent_topics_df = pd.concat([sent_topics_df, contents], axis=1)
    return(sent_topics_df)
    
def getTopicDistribution(df_topic_analysis):
    # Format
    df_dominant_topic = df_topic_analysis.reset_index()
    df_dominant_topic.columns = ['Document_No', 'Dominant_Topic', 'Topic_Perc_Contrib', 'Keywords', 'Text']
    return df_dominant_topic


def get_topic_analysis_dataframe(df_topic_analysis,df,folder):
    ## Topic Distribution
    df_topic_distribution = getTopicDistribution(df_topic_analysis)

    df_topic_distribution_full=pd.concat([df_topic_distribution.set_index('Document_No'),df], axis=1, join='inner')    
    df_topic_distribution_full.to_pickle(folder + '/df_topic_distribution_full')

    #--------------------------
    ## MOST REPRESENTATIVE DOCUMENT PER TOPIC
    #--------------------------
    # Group top 5 sentences under each topic
    df_best_score_topic_document=df_topic_distribution.loc[df_topic_distribution.reset_index().groupby(['Dominant_Topic'])['Topic_Perc_Contrib'].idxmax()]
    df_best_score_topic_document=pd.concat([df_best_score_topic_document.set_index('Document_No'),df], axis=1, join='inner')    
    df_best_score_topic_document.to_pickle(folder+'/df_best_score_topic_document')
    
    # Classement des meilleures années par topics        
    df_per_year_results = pd.concat([df_topic_distribution.set_index('Document_No'),df], axis=1, join='inner')    
    df_per_year_results = df_per_year_results[['Dominant_Topic', 'Year','Song']].groupby(['Dominant_Topic', 'Year'],as_index=False).count()
    df_per_year_results['SUM']=df_per_year_results.groupby('Dominant_Topic')['Song'].transform('sum')
    df_per_year_results.to_pickle(folder+'/df_per_year_results')

    # Répartition des topics par année
    df_best_year_per_topic = df_per_year_results.loc[df_per_year_results.groupby(["Dominant_Topic"])['Song'].idxmax()]
    df_best_year_per_topic.to_pickle(folder+'/df_best_year_per_topic')



def createWordCloudImages(dataframe,column_groupby,column_text_name):
    grouped = dataframe.groupby(['Year'])
    print("Génération des wordclouds")
    # Calculate iteration needed
    pbar = tqdm.tqdm(total=len(grouped))
    for year,group in grouped:
        results= grouped.get_group(year)['Cleaned_Lyrics']
        results= ' '.join(results)
        wordcloud = WordCloud().generate(results)
        wordcloud.to_file(PATH_WORDCLOUD + '/WC_Lyrics_'+str(year)+".png")
        # Progress bar update
        pbar.update(1)
    # Close progress bar
    pbar.close()

def updateWorcloudImgYear(dataframe,column_groupby,column_text_name,year_request):
    grouped = dataframe.groupby(['Year'])
    print("Génération des wordclouds")
    for year,group in grouped:
        if(int(year)==int(year_request)):
            results= grouped.get_group(year)['Cleaned_Lyrics']
            results= ' '.join(results)
            wordcloud = WordCloud().generate(results)
            wordcloud.to_file(PATH_WORDCLOUD + '/WC_Lyrics_'+str(year)+".png")
            break

##############################
## FONCTIONS LYRICS TEST
##############################
def analyse_lyrics(lda_model,model_name,song_name,lyrics,id2word,only_noun):

    ## Prepare lyrics data (tokenize,stopwords,lemmatize)
    data_words = list(sent_to_words(lyrics))

    # Build the bigram and trigram models
    bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100)  # higher threshold fewer phrases.
    #trigram = gensim.models.Phrases(bigram[data_words], threshold=100)

    # Faster way to get a sentence clubbed as a trigram/bigram
    bigram_mod = gensim.models.phrases.Phraser(bigram)
    #trigram_mod = gensim.models.phrases.Phraser(trigram)

    # Remove Stop Words
    data_words_nostops = remove_stopwords(data_words)

    # Form Bigrams
    data_words_bigrams = make_bigrams(bigram_mod,data_words_nostops)

    # Do lemmatization keeping only noun, adj, vb, adv
    if only_noun: 
        data_lemmatized = lemmatization(data_words_bigrams, allowed_postags=['NOUN'])
    else:
        data_lemmatized = lemmatization(data_words_bigrams, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV'])
    
    unseen_docs = [id2word.doc2bow(text) for text in data_lemmatized][0]
    vector = lda_model[unseen_docs]

    print("####################################################")
    print("Chanson  : " + song_name)
    print("Modèle   : " + model_name)
    print("####################################################")
    if(isinstance(vector[0],tuple)):
        for index, score in sorted(vector, key=lambda tup: -1*tup[1]):
            print("Score: {}\t Topic: {}".format(score, lda_model.print_topic(index, 5)))
    elif(isinstance(vector[0], list)):
        for index, score in sorted(vector[0], key=lambda tup: -1*tup[1]):
            print("Score: {}\t Topic: {}".format(score, lda_model.print_topic(index, 5)))
    print("####################################################")
    return vector

##############################
## FONCTIONS FLASK API
##############################

def flask_test_folder(root_path):
    # Checking if model already saved 
    if os.path.isfile(os.path.join(root_path,PATH_LDA_MODEL,"lda_model_basic")):
        return True
    return False

def get_all_data(root_path):
    # Dictionnary id2word
    id2word = corpora.Dictionary.load(os.path.join(root_path,PATH_DATA,"id2word"))
    # Corpus
    with open(os.path.join(root_path,PATH_DATA,"corpus"), 'rb') as f:
        corpus = pickle.load(f)
    # Data_lemmatized
    with open(os.path.join(root_path,PATH_DATA,"data_lemmatized"), 'rb') as f:
        data_lemmatized = pickle.load(f)
    
    return id2word,corpus,data_lemmatized


def get_all_dataframe(root_path,model):
    path_folder = "" 
    if(model == "LDA"):
        path_folder = os.path.join(root_path,PATH_LDA_MODEL)
    elif(model=="LSA"):
        path_folder = os.path.join(root_path,PATH_LSA_MODEL)
    elif(model=="LDA_MALLET"):
        path_folder = os.path.join(root_path,PATH_MALLET_MODEL)
    
    # Best Score Topic Document 
    df_best_score_topic_document = pd.read_pickle(os.path.join(path_folder,"df_best_score_topic_document"))
    # Best Year per Topic
    df_best_year_per_topic = pd.read_pickle(os.path.join(path_folder,"df_best_year_per_topic"))
    # Per year Topic result 
    df_per_year_results = pd.read_pickle(os.path.join(path_folder,"df_per_year_results"))
    # Topic Distribution
    df_topic_distribution_full = pd.read_pickle(os.path.join(path_folder,"df_topic_distribution_full"))

    return df_best_score_topic_document,df_best_year_per_topic,df_per_year_results,df_topic_distribution_full
    
def analyse_lyrics_flask(data,only_noun,root_path):
    # VARIABLES 
    lda_model = None
    lyrics = [data['lyrics']]
    
    # Loading Dictionnary
    if os.path.isfile(os.path.join(root_path,PATH_DATA,"id2word")):
        # Loading dictionary
        id2word = corpora.Dictionary.load(os.path.join(root_path,PATH_DATA,"id2word"))
    else: 
        return "No dictionnary available"

    # Loading Model
    if(data['model'] =='LDA'):    
        # Checking if model already saved 
        if os.path.isfile(os.path.join(root_path,PATH_LDA_MODEL,"lda_model_basic")):
            print ("Model already exist, we'll use it.")
            lda_model = gensim.models.LdaMulticore.load(os.path.join(root_path,PATH_LDA_MODEL,"lda_model_basic"))
        else:
            return "No saved model, create it before"

    elif(data['model'] =='LSA'):    
        # Checking if model already saved 
        if os.path.isfile(os.path.join(root_path,PATH_LSA_MODEL,"lsa_model_basic")):
            print ("Model already exist, we'll use it.")
            lda_model = gensim.models.LsiModel.load(os.path.join(root_path,PATH_LSA_MODEL,"lsa_model_basic"))
        else:
            return "No saved model, create it before"

    elif(data['model'] =='LDA_MALLET'):
        # Checking if model already saved 
        if os.path.isfile(os.path.join(root_path,PATH_MALLET_MODEL,"lda_mallet_basic")):
            print ("Model already exist, we'll use it.")
            lda_model = gensim.models.wrappers.LdaMallet.load(os.path.join(root_path,PATH_MALLET_MODEL,"lda_mallet_basic"))
        else:
            return "No saved model, create it before"

    ## Prepare lyrics data (tokenize,stopwords,lemmatize)
    data_words = list(sent_to_words(lyrics))

    # Build the bigram and trigram models
    bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100)  # higher threshold fewer phrases.
    #trigram = gensim.models.Phrases(bigram[data_words], threshold=100)

    # Faster way to get a sentence clubbed as a trigram/bigram
    bigram_mod = gensim.models.phrases.Phraser(bigram)
    #trigram_mod = gensim.models.phrases.Phraser(trigram)

    # Remove Stop Words
    data_words_nostops = remove_stopwords(data_words)

    # Form Bigrams
    data_words_bigrams = make_bigrams(bigram_mod,data_words_nostops)

    # Do lemmatization keeping only noun, adj, vb, adv
    if only_noun: 
        data_lemmatized = lemmatization(data_words_bigrams, allowed_postags=['NOUN'])
    else:
        data_lemmatized = lemmatization(data_words_bigrams, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV'])

    unseen_docs = [id2word.doc2bow(text) for text in data_lemmatized][0]
    vector = lda_model[unseen_docs]

    #######################
    # PREPARE JSON DATA
    #######################
    result= {}
    for attr, value in data.items():
        result[attr]=value
    result["result"]=[]

    if(isinstance(vector[0],tuple)):
        for index, score in sorted(vector, key=lambda tup: -1*tup[1]):
             if(score > 0):
                result["result"].append({"Index":str(index),"Score":str(score),"Topics":str(lda_model.print_topic(index, 5))})
    elif(isinstance(vector[0], list)):
        for index, score in sorted(vector[0], key=lambda tup: -1*tup[1]):
            if(score > 0): 
                result["result"].append({"Index":str(index),"Score":str(score),"Topics":str(lda_model.print_topic(index, 5))})


    # Return JSON result 
    return result