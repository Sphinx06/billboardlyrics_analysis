import functions as fc

from functions import gensim
from functions import os

# RESSOURCES
PATH_LDA_MODEL = os.path.join("static","ressources","lda_model")
PATH_LSA_MODEL = os.path.join("static","ressources","lda_model")
PATH_MALLET_MODEL = os.path.join("static","ressources","mallet_model")
PATH_MALLET_BIN = 'mallet-2.0.8/bin/mallet'
PATH_DATA = os.path.join("static","ressources","data")
PATH_WORDCLOUD = os.path.join("static","img","wordcloud")

##############################
# ALL MODELS 
##############################

# LDA MODEL CLASS
class LDAModel():
    # Contructor
    def __init__(self,corpus,id2word,random_state=100,chunksize=100,passes=10,per_word_topics=True):
        self.path = PATH_LSA_MODEL
        self.model_path_name = os.path.join(self.path,'lda_model_basic')
        self.model = None
        self.corpus = corpus
        self.id2word = id2word
        self.random_state = random_state
        self.chunksize = chunksize
        self.passes = passes
        self.per_word_topics = per_word_topics
    
    # Setter
    def setCorpus(self,corpus):
        self.corpus = corpus

    def setDictionnary(self,id2word):
        self.id2word = id2word

    # Create and save model
    def create_save_model(self,num_topics):
        if self.corpus and self.id2word:
            # LDA model generation 
            self.model = gensim.models.LdaMulticore(corpus=self.corpus,
                                                id2word=self.id2word,
                                                num_topics=7,
                                                random_state=100,
                                                chunksize=100,
                                                passes=10,
                                                per_word_topics=True)
            self.model.save(self.model_path_name)
        else:
            print('Corpus or dictionnary not set.')
            self.model = None

    # Load model 
    def load_model(self):
        if os.path.isfile(self.model_path_name):
            self.model = gensim.models.LsiModel.load(self.model_path_name)
        else:
            print('Model not found')
            self.model = None


# LSA MODEL CLASS
class LSAModel():
    # Contructor
    def __init__(self,corpus,id2word):
        self.path = PATH_LSA_MODEL
        self.model_path_name = os.path.join(self.path,'lsa_model_basic')
        self.model = None
        self.corpus = corpus
        self.id2word = id2word
    
    # Setter
    def setCorpus(self,corpus):
        self.corpus = corpus

    def setDictionnary(self,id2word):
        self.id2word = id2word

    # Create and save model
    def create_save_model(self,num_topics):
        if self.corpus and self.id2word:
            # LDA model generation 
            self.model = gensim.models.LsiModel(corpus=self.corpus,
                                                id2word=self.id2word,
                                                num_topics=num_topics)
            self.model.save(self.model_path_name)
        else:
            print('Corpus or dictionnary not set.')
            self.model = None

    # Load model 
    def load_model(self):
        if os.path.isfile(self.model_path_name):
            self.model = gensim.models.LsiModel.load(self.model_path_name)
        else:
            print('Model not found')
            self.model = None

# MALLET MODEL CLASS
class MALLETModel():
    # Contructor
    def __init__(self,corpus,id2word):
        self.path = PATH_MALLET_MODEL
        self.mallet_path = PATH_MALLET_BIN
        self.model_path_name = os.path.join(self.path,'lda_mallet_basic')
        self.model = None
        self.corpus = corpus
        self.id2word = id2word
    
    # Setter
    def setCorpus(self,corpus):
        self.corpus = corpus

    def setDictionnary(self,id2word):
        self.id2word = id2word

    # Create and save model
    def create_save_model(self,num_topics):
        if self.corpus and self.id2word:
            # LDA model generation 
            self.model =  gensim.models.wrappers.LdaMallet(self.mallet_path,corpus=self.corpus,num_topics=num_topics,id2word=self.id2word,prefix=self.path)
            self.model.save(self.model_path_name)
        else:
            print('Corpus or dictionnary not set.')
            self.model = None

    # Load model 
    def load_model(self):
        if os.path.isfile(self.model_path_name):
            self.model = gensim.models.wrappers.LdaMallet.load(self.model_path_name)
        else:
            print('Model not found')
            self.model = None
