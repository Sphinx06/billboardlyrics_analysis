#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##############################
# GLOBAL VARIABLES
##############################
WORDCLOUD_CREATE_IMAGES = False
POS_TAG_ONLY_NOUNS = False
OVERRIDE_MODEL = False
MALLET_PATH = './mallet-2.0.8/bin/mallet'
 	
##############################
# LIBRARIES
##############################
import functions as fc
import re
import numpy as np
import os.path

# Plotting tools
import pyLDAvis
import pyLDAvis.gensim  # don't skip this
import matplotlib.pyplot as plt

from functions import gensim
from functions import corpora
from functions import CoherenceModel
from functions import pd 
from gensim.test.utils import datapath
from os.path import join
import pickle


##############################
## MAIN
##############################
os.chdir(os.path.dirname(os.path.realpath(__file__)))

# Read data from file 'filename.csv'
df = fc.pd.read_csv("https://raw.githubusercontent.com/walkerkq/musiclyrics/master/billboard_lyrics_1964-2015.csv",
                 encoding='latin-1').dropna()

##############################
# Preprocessing data
##############################

# Cleaning empty lyrics
df['Cleaned_Lyrics'] = df['Lyrics'].map(lambda x: re.sub('[,\.!?]', '', x))
# Remove new line characters
df['Cleaned_Lyrics'] = df['Cleaned_Lyrics'].map(lambda x: re.sub('\s+', ' ', x))
df['Cleaned_Lyrics'] = df['Cleaned_Lyrics'].map(lambda x: x.lower())
df['Cleaned_Lyrics'].dropna()
df = df[df['Cleaned_Lyrics'].str.lower().str.split().map(len) > 1]
data = df['Cleaned_Lyrics'].values.tolist()

## Prepare lyrics data (tokenize,stopwords,lemmatize)
data_words = list(fc.sent_to_words(data))

# Create wordcloud images
if WORDCLOUD_CREATE_IMAGES:
    fc.createWordCloudImages(df,'Year','Cleaned_Lyrics')

##############################
# Integration of bigram and trigram models
##############################
# Build the bigram and trigram models
bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100)  # higher threshold fewer phrases.
trigram = gensim.models.Phrases(bigram[data_words], threshold=100)

# Faster way to get a sentence clubbed as a trigram/bigram
bigram_mod = gensim.models.phrases.Phraser(bigram)
trigram_mod = gensim.models.phrases.Phraser(trigram)

# See trigram example
#print(trigram_mod[bigram_mod[data_words[0]]])

# Remove Stop Words
data_words_nostops = fc.remove_stopwords(data_words)

# Form Bigrams
data_words_bigrams = fc.make_bigrams(bigram_mod,data_words_nostops)

# Do lemmatization keeping only noun, adj, vb, adv
if POS_TAG_ONLY_NOUNS: 
    data_lemmatized = fc.lemmatization(data_words_bigrams, allowed_postags=['NOUN'])
else:
    data_lemmatized = fc.lemmatization(data_words_bigrams, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV'])

##############################
# LDA MODEL
##############################

# =============================
# STEP 1: CREATE AND SAVE DATA
# =============================

# Create Dictionary
id2word = corpora.Dictionary(data_lemmatized)
# Term Document Frequency
corpus = [id2word.doc2bow(text) for text in data_lemmatized]

# Save data
id2word.save(join(fc.PATH_DATA,'id2word'))
with open(join(fc.PATH_DATA,'corpus'), 'wb') as f1:
    pickle.dump(corpus, f1)
with open(join(fc.PATH_DATA,'data_lemmatized'), 'wb') as f2:
    pickle.dump(data_lemmatized, f2)

# ===============================================
# STEP 2: FIND OPTIMIZED HYPERPARAMETERS MODEL
# ===============================================
FINDING_HYPERPARAMETERS_ENABLE = False

# Can take a long time to run
if FINDING_HYPERPARAMETERS_ENABLE:
    ''' 
      You need to define how far you what to search :
      min_topics = minimum topics.
      max_topics = maximum topics.
      alpha = range of alpha to try.
      beta = range of alpha to try.
      corpus_sets = you can add part of the corpus to test on a sample.
    '''

    grid = {}
    grid['Validation_Set'] = {}
    # Topics range
    min_topics = 5
    max_topics = 15
    step_size = 1
    topics_range = range(min_topics, max_topics, step_size)

    # Alpha parameter
    alpha = list(np.arange(0.01, 1, 0.3))
    alpha.append('symmetric')
    alpha.append('asymmetric')

    # Beta parameter
    beta = list(np.arange(0.01, 1, 0.3))
    beta.append('symmetric')

    # Validation sets
    num_of_docs = len(corpus)
    #print(num_of_docs)
    corpus_sets = [  # gensim.utils.ClippedCorpus(corpus, round(num_of_docs*0.75)),
        corpus]

    corpus_title = [  # '75% Corpus',
        '100% Corpus']
    # Launch finding
    fc.finding_hyperparameters(id2word, corpus_sets,data_lemmatized, topics_range, alpha, beta, True)

# =============================
# STEP 2 BIS: FIND TOPICS 
# =============================
else:
    # -------------------------
    ### LDA MODEL
    #--------------------------
    print("###### MODEL LDA ########")
    lda_model = None
    OVERRIDE_MODEL = True
    # Checking if model already saved 
    if not OVERRIDE_MODEL and os.path.isfile(join(fc.PATH_LDA_MODEL,'lda_model_basic')):
        print ("Model already exist, we'll use it.")
        lda_model = gensim.models.LdaMulticore.load(join(fc.PATH_LDA_MODEL,'lda_model_basic'))
    else:
        print ("No saved model, create a basic one")
        # LDA model generation 
        lda_model = gensim.models.LdaMulticore(corpus=corpus,
                                               id2word=id2word,
                                               num_topics=7,
                                               random_state=100,
                                               chunksize=100,
                                               passes=10,
                                               per_word_topics=True)
        lda_model.save(join(fc.PATH_LDA_MODEL,'lda_model_basic'))
    
    
    #--------------------------
    ### TOPICS
    #--------------------------
    ## Checking topics rendered by the ldamodel
    topics = lda_model.print_topics(num_words=10)

    ## Show topics generated
    for topic in topics:
        print(topic)

    #--------------------------
    ### COHERENCE SCORE
    #--------------------------
    # Compute Coherence Score
    coherence_model_lda = CoherenceModel(model=lda_model, texts=data_lemmatized, dictionary=id2word, coherence='c_v')
    coherence_lda = coherence_model_lda.get_coherence()
    print('\nCoherence Score: ', coherence_lda)

    #--------------------------
    ### ANALYSIS TOPICS RESULTS
    #--------------------------
    # Récupération des résultats par documents 
    df_topic_analysis = fc.format_topics_sentences(lda_model, corpus, data_lemmatized)
    
    ## Topic Distribution
    df_topic_distribution = fc.getTopicDistribution(df_topic_analysis)

    df_topic_distribution_full=pd.concat([df_topic_distribution.set_index('Document_No'),df], axis=1, join='inner')    
    df_topic_distribution_full.to_pickle(join(fc.PATH_LDA_MODEL,'df_topic_distribution_full'))

    #--------------------------
    ## MOST REPRESENTATIVE DOCUMENT PER TOPIC
    #--------------------------
    # Group top 5 sentences under each topic
    df_best_score_topic_document=df_topic_distribution.loc[df_topic_distribution.reset_index().groupby(['Dominant_Topic'])['Topic_Perc_Contrib'].idxmax()]
    df_best_score_topic_document=pd.concat([df_best_score_topic_document.set_index('Document_No'),df], axis=1, join='inner')    
    df_best_score_topic_document.to_pickle(join(fc.PATH_LDA_MODEL,'df_best_score_topic_document'))
    
    # Classement des meilleures années par topics        
    df_per_year_results = pd.concat([df_topic_distribution.set_index('Document_No'),df], axis=1, join='inner')    
    df_per_year_results = df_per_year_results[['Dominant_Topic', 'Year','Song']].groupby(['Dominant_Topic', 'Year'],as_index=False).count()
    df_per_year_results['SUM']=df_per_year_results.groupby('Dominant_Topic')['Song'].transform('sum')
    df_per_year_results.to_pickle(join(fc.PATH_LDA_MODEL,'df_per_year_results'))

    # Répartition des topics par année
    df_best_year_per_topic = df_per_year_results.loc[df_per_year_results.groupby(["Dominant_Topic"])['Song'].idxmax()]
    df_best_year_per_topic.to_pickle(join(fc.PATH_LDA_MODEL,'df_best_year_per_topic'))
    
    #df_best_year_per_topic.plot.pie(labels=range(0,len(df_best_year_per_topic)),y='SUM',figsize=(5, 5),autopct='%1.1f%%', startangle=90)
    #plt.title("Répartition par année")
    #plt.show()

    #df_best_year_per_topic.plot(kind='scatter',x='Dominant_Topic',y='Year',color='red')
    #plt.show()
    
    # TEST NEW DOC TOPICS
    #lyrics = ["Look, my bitches all bad, my niggas all real  I ride on his dick in some big tall heels  Big fat checks, big large bills  Front, I'll flip like ten cartwheels  Cold ass bitch, I give broads chills   Ten different looks and my looks all kill  I kiss him in the mouth, I feel all grills  He eat in the car, that's meals on wheels (Woo)   I was born to flex (Yes)  Diamonds on my neck  I like boardin' jets, I like mornin' sex (Woo)  But nothing in this world that I like more than checks (Money)  All I really wanna see is the (Money)  I don't really need the D, I need the (Money)  All a bad bitch need is the (Money flow)  I got bands in the coupe (Coupe)  Bustin' out the roof  I got bands in the coupe (Coupe)  Touch me, I'll shoot (Bow)  Shake a lil ass (Money)  Get a little bag and take it to the store (Store, money)   Get a little cash (Money)  Shake it real fast and get a little more (Money)  I got bands in the coupe (Coupe)  Bustin' out the roof  I got bands in the coupe (Brrr)  Bustin' out the roof (Cardi)   I gotta fly, I need a jet, shit  I need room for my legs  I got a baby, I need some money, yeah  I need cheese for my egg  All y'all bitches in trouble  Bring brass knuckles to the scuffle  I heard that Cardi went pop  Yeah, I did go pop (Pop)  That's me bustin' they bubble  I'm Dasani with the drip  Baby mommy with the clip   Walk out Follie's with a bitch  Bring a thottie to the whip  If she fine or she thick, goddamn  Walkin' past the mirror, ooh  Damn, I'm fine (Fine)  Let a bitch try me, boom (Boom)  Hammer time, uh   I was born to flex (Yes)  Diamonds on my neck  I like boardin' jets, I like mornin' sex (Woo)  But nothing in this world that I like more than checks (Money)  All I really wanna see is the (Money)  I don't really need the D, I need the (Money)  All a bad bitch need is the (Money flow)  I got bands in the coupe (Coupe)  Bustin' out the roof   I got bands in the coupe (Coupe)  Touch me, I'll shoot (Bow)  Shake a lil ass (Money)  Get a little bag and take it to the store (Store, money)  Get a little cash (Money)  Shake it real fast and get a little more (Money)  I got bands in the coupe (Coupe)  Bustin' out the roof  I got bands in the coupe (Brrr)  Touch me, I'll shoot (Bow)   Bitch, I will pop on your pops (Your pops)  Bitch, I will pop on whoever (Brrr)  You know who pop the most shit? (Who?)  The people whose shit not together (Okay)  You'da bet Cardi a freak (Freak)  All my pajamas is leather (Uh)   Bitch, I will black on your ass (Yeah)  Wakanda forever  Sweet like a honey bun, spit like a Tommy gun  Rollie a one of one, come get your mommy some  Cardi at the tip-top, bitch  Kiss the ring and kick rocks, sis (Mwah)  Jump it down, back it up (Ooh, ayy)  Make that nigga put down 2K  I like my niggas dark like D'USSÉ  He gonna eat this ass like soufflé   I was born to flex, diamonds on my neck  I like boardin' jets, I like mornin' sex  But nothing in this world that I like more than Kulture  (Kulture, Kulture, Kulture)  All I really wanna see is the (Money)  I don't really need the D, I need the (Money)   All a bad bitch need is the  K.K.C (Woo)   (Money)  Money  (Money)  (Money)  (Money)  (Money)  (Money)  (Money) "]
    #vector = fc.analyse_lyrics(lda_model,"LDA","Money - Carli B",lyrics,id2word,False)


    #-----------------------------
    ## LSA
    #----------------------------- 
    print("###### MODEL LSA ########")
    lsa_model = None
    OVERRIDE_MODEL = True

    # Checking if model already saved 
    if not OVERRIDE_MODEL and os.path.isfile(join(fc.PATH_LSA_MODEL,'lsa_model_basic')):
        print ("Model already exist, we'll use it.")
        lsa_model = gensim.models.LsiModel.load(join(fc.PATH_LSA_MODEL,'lsa_model_basic'))
    else:
        print ("No saved model, create a basic one")
        # LDA model generation 
        lsa_model = gensim.models.LsiModel(corpus, num_topics=10, id2word = id2word)
        lsa_model.save(join(fc.PATH_LSA_MODEL,'lsa_model_basic'))


    #--------------------------
    ### TOPICS
    #--------------------------
    ## Checking topics rendered by the ldamodel
    topics = lsa_model.print_topics(num_words=10)

    ## Show topics generated
    for topic in topics:
        print(topic)

    #--------------------------
    ### COHERENCE SCORE
    #--------------------------
    # Compute Coherence Score
    coherence_model_lsa = CoherenceModel(model=lsa_model, texts=data_lemmatized, dictionary=id2word, coherence='c_v')
    coherence_lsa = coherence_model_lsa.get_coherence()
    print('\nCoherence Score: ', coherence_lsa)

    #--------------------------
    ### ANALYSIS TOPICS RESULTS
    #--------------------------
    df_topic_analysis = None
    df_topic_distribution = None
    df_best_score_topic_document = None
    df_per_year_results = None
    df_best_year_per_topic = None
    
    # Récupération des résultats par documents 
    df_topic_analysis = fc.format_topics_sentences_mallet(lsa_model, corpus, data_lemmatized)
    
    ## Topic Distribution
    df_topic_distribution = fc.getTopicDistribution(df_topic_analysis)

    df_topic_distribution_full=pd.concat([df_topic_distribution.set_index('Document_No'),df], axis=1, join='inner')    
    df_topic_distribution_full.to_pickle(join(fc.PATH_LSA_MODEL,'df_topic_distribution_full'))

    #--------------------------
    ## MOST REPRESENTATIVE DOCUMENT PER TOPIC
    #--------------------------
    # Group top 5 sentences under each topic
    df_best_score_topic_document=df_topic_distribution.loc[df_topic_distribution.reset_index().groupby(['Dominant_Topic'])['Topic_Perc_Contrib'].idxmax()]
    df_best_score_topic_document=pd.concat([df_best_score_topic_document.set_index('Document_No'),df], axis=1, join='inner')    
    df_best_score_topic_document.to_pickle(join(fc.PATH_LSA_MODEL,'df_best_score_topic_document'))
    
    # Classement des meilleures années par topics        
    df_per_year_results = pd.concat([df_topic_distribution.set_index('Document_No'),df], axis=1, join='inner')    
    df_per_year_results = df_per_year_results[['Dominant_Topic', 'Year','Song']].groupby(['Dominant_Topic', 'Year'],as_index=False).count()
    df_per_year_results['SUM']=df_per_year_results.groupby('Dominant_Topic')['Song'].transform('sum')
    df_per_year_results.to_pickle(join(fc.PATH_LSA_MODEL,'df_per_year_results'))

    # Répartition des topics par année
    df_best_year_per_topic = df_per_year_results.loc[df_per_year_results.groupby(["Dominant_Topic"])['Song'].idxmax()]
    df_best_year_per_topic.to_pickle(join(fc.PATH_LSA_MODEL,'df_best_year_per_topic'))
    
    #-----------------------------
    ## LDA MALLET
    #----------------------------- 
    print("###### MODEL LDA MALLET ########")
    # Can take a long time to run
    if FINDING_HYPERPARAMETERS_ENABLE:
         # Can take a long time to run.
        model_list, coherence_values = fc.compute_coherence_values_ldamallet(MALLET_PATH,dictionary=id2word, corpus=corpus, texts=data_lemmatized, start=2, limit=40, step=6)
        
        # Show graph
        limit=40; start=2; step=4;
        x = range(start, limit, step)
        plt.plot(x, coherence_values)
        plt.xlabel("Num Topics")
        plt.ylabel("Coherence score")
        plt.legend(("coherence_values"), loc='best')
        plt.show()
        
        # Print the coherence scores
        for m, cv in zip(x, coherence_values):
            print("Num Topics =", m, " has Coherence Value of", round(cv, 4))
        best_coherence_model_index = coherence_values.index(max(coherence_values))
        
        # Select the model and print the topics
        optimal_model = model_list[best_coherence_model_index]
        model_topics = optimal_model.show_topics(formatted=False)
        print(optimal_model.print_topics(num_words=10))
    else:
        lda_mallet = None
        OVERRIDE_MODEL = False
        # Checking if model already saved 
        if not OVERRIDE_MODEL and os.path.isfile(join(fc.PATH_MALLET_MODEL,'lda_mallet_basic')):
            print ("Model already exist, we'll use it.")
            lda_mallet = gensim.models.wrappers.LdaMallet.load(join(fc.PATH_MALLET_MODEL,'lda_mallet_basic'))
        else:
            print ("No saved model, create a basic one")
            # LDA model generation 
            lda_mallet =  gensim.models.wrappers.LdaMallet(MALLET_PATH,corpus=corpus,num_topics=10,id2word=id2word,prefix=fc.PATH_MALLET_MODEL)
            lda_mallet.save(join(fc.PATH_MALLET_MODEL,'lda_mallet_basic'))
        
        
         # Show Topics
        print(lda_mallet.show_topics(formatted=False))
        
        # Compute Coherence Score
        coherence_model_ldamallet = CoherenceModel(model=lda_mallet, texts=data_lemmatized, dictionary=id2word, coherence='c_v')
        coherence_ldamallet = coherence_model_ldamallet.get_coherence()
        print('\nCoherence Score: ', coherence_ldamallet)
        
   
        #df_topic_sents_keywords_mallet = fc.format_topics_sentences_mallet(lda_mallet, corpus, data_lemmatized)

        #lda_mallet.get_document_topics
        #vector2 = fc.analyse_lyrics(lda_mallet,"LDA_MALLET","Money - Carli B",lyrics,id2word,False)

        df_topic_analysis = None
        df_topic_distribution = None
        df_best_score_topic_document = None
        df_per_year_results = None
        df_best_year_per_topic = None
        
        # Récupération des résultats par documents 
        df_topic_analysis = fc.format_topics_sentences_mallet(lda_mallet, corpus, data_lemmatized)
        
        ## Topic Distribution
        df_topic_distribution = fc.getTopicDistribution(df_topic_analysis)

        df_topic_distribution_full=pd.concat([df_topic_distribution.set_index('Document_No'),df], axis=1, join='inner')    
        df_topic_distribution_full.to_pickle(join(fc.PATH_MALLET_MODEL,'df_topic_distribution_full'))

        #--------------------------
        ## MOST REPRESENTATIVE DOCUMENT PER TOPIC
        #--------------------------
        # Group top 5 sentences under each topic
        df_best_score_topic_document=df_topic_distribution.loc[df_topic_distribution.reset_index().groupby(['Dominant_Topic'])['Topic_Perc_Contrib'].idxmax()]
        df_best_score_topic_document=pd.concat([df_best_score_topic_document.set_index('Document_No'),df], axis=1, join='inner')    
        df_best_score_topic_document.to_pickle(join(fc.PATH_MALLET_MODEL,'df_best_score_topic_document'))
        
        # Classement des meilleures années par topics        
        df_per_year_results = pd.concat([df_topic_distribution.set_index('Document_No'),df], axis=1, join='inner')    
        df_per_year_results = df_per_year_results[['Dominant_Topic', 'Year','Song']].groupby(['Dominant_Topic', 'Year'],as_index=False).count()
        df_per_year_results['SUM']=df_per_year_results.groupby('Dominant_Topic')['Song'].transform('sum')
        df_per_year_results.to_pickle(join(fc.PATH_MALLET_MODEL,'df_per_year_results'))

        # Répartition des topics par année
        df_best_year_per_topic = df_per_year_results.loc[df_per_year_results.groupby(["Dominant_Topic"])['Song'].idxmax()]
        df_best_year_per_topic.to_pickle(join(fc.PATH_MALLET_MODEL,'df_best_year_per_topic'))