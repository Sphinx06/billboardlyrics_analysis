# BillboardLyrics Analysis

Topic analysis training (AI)

# PREREQUIREMENTS

Follow bellow step to use the project.
You must be connected to internet to ba able to use it. (download library/js/etc..)

## LIBRARY INSTALLATION

1. Install library for python3 (you must have it installed on your OS)
```
pip3 install tqdm spacy gensim nltk pyLDAvis wordcloud flask 
```
or
```
pip3 install tqdm
pip3 install spacy
pip3 install gensim
pip3 install nltk
pip3 install pyLDAvis
pip3 install wordcloud
pip3 install flask
```
2. Download dictionnary
```
python3 -m spacy download en
```

## LAUNCH FLASK SERVER (WEB APP)
From the root of the project
```
python3 app.py
```

## LAUNCH TRAINING
From the root of the project
```
python3 app_training.py
```