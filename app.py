from flask import Flask, request, render_template,jsonify,abort
import functions as HGIMS
import os.path

# Init Flask
app = Flask(__name__)

# ==========================================
# Functions Flask
# ==========================================
# LAST.FM
# API KEY : bb9643add015f76bceb3a171e023678b
# GET INFO 
# http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=bb9643add015f76bceb3a171e023678b&artist=cher&track=believe&format=json

def analyse_lyrics(data):
    # Checking inputs
    if data['lyrics'] and data['lyrics'] != ""and data['song'] and data['song'] != "" and data['model'] and data['model'] != "":
        # Checking folder 
        if HGIMS.flask_test_folder(root_path = app.root_path):
            # Get values 
            value = HGIMS.analyse_lyrics_flask(data,False,app.root_path)
            return value
        else :
            raise "TEST FOLDER FAILED FOR FLASK"
        raise "PROBLEM WITH THE DATA (LYRICS OR SONG OR MODEL)"


# ==========================================
# Routes
# ==========================================
### ERROR HANDLING 
@app.errorhandler(400)
def custom400(error):
    response = jsonify({'message': error.description})
    response.status_code = 400
    return response

### Pages
# Home page
@app.route('/')
def home():
    return render_template('index.html')

@app.route('/analysis',methods=['GET','POST'])
def analysis():
    if request.method == 'POST':
        song = request.form.get('song')
        lyrics = request.form.get('lyrics')
        artist = request.form.get('artist')
        return render_template('analysis.html',song=song,lyrics=lyrics,artist=artist)
    elif request.method == 'GET':
        return render_template('analysis.html',song='',lyrics='',artist='')

### Services
# Analyse Lyrics
@app.route('/analyseLyrics', methods=['POST'])
def service_analyse():
    # Getting request in JSON FORMAT
    data = request.get_json()

    try:
        # Getting topics from model using given lyrics,song
        analyse = analyse_lyrics(data)
        df_best_score_topic_document,df_best_year_per_topic,df_per_year_results,df_topic_distribution_full = HGIMS.get_all_dataframe(app.root_path,data['model'])

        # Best topic index of lyrics
        best_topic_index = int(analyse['result'][0]['Index'])

        # Get 5 first song of the dominant topic (order by percent contribution)
        df_best_songs_contrib_best_topic = df_topic_distribution_full[df_topic_distribution_full['Dominant_Topic']==best_topic_index].sort_values(by=['Topic_Perc_Contrib'],ascending=False).head(10)
        df_best_songs_rank_best_topic = df_topic_distribution_full[df_topic_distribution_full['Dominant_Topic']==best_topic_index].sort_values(by=['Rank','Topic_Perc_Contrib'],ascending=[True,False]).head(10)

        analyse['df_best_score_topic_document'] = df_best_score_topic_document.to_dict(orient='records')
        analyse['df_best_year_per_topic'] = df_best_year_per_topic.to_dict(orient='records')
        analyse['df_per_year_results'] = df_per_year_results.to_dict(orient='records')
        analyse['df_best_songs_contrib_best_topic'] = df_best_songs_contrib_best_topic.to_dict(orient='records')
        analyse['df_best_songs_rank_best_topic'] = df_best_songs_rank_best_topic.to_dict(orient='records')
        analyse['best_topic_id'] = best_topic_index

        # Get other info 
        return jsonify(analyse)
        
    # ERROR HANDLER
    except Exception as ex:
        print("[ERROR] ==> SERVICE ANALYSELYRICS ==> PROBLEME SUIVANT: {0}".format(ex))
        return jsonify(error=True,result="[ERROR] ==> SERVICE ANALYSELYRICS ==> PROBLEME SUIVANT: {0}".format(ex))   
        #abort(400,"ERREUR ANALYSE LYRICS")
    

# MAIN
if __name__ == '__main__':
    app.run(debug=True)